let blockBase = document.getElementById("base");

//Задание 1
let titles = blockBase.querySelectorAll(".header-four");
for (let title of titles) {
  title.textContent = title.textContent.toUpperCase();
}

//Задание 2

let subTitles = blockBase.querySelectorAll(".text-middle");
for (let subTitle of subTitles) {
  if (subTitle.textContent.length > 20) {
    let text = subTitle.textContent.substring(0, 20);
    subTitle.innerHTML = `${text}...`;
  }
}

//ДЗ 7

const isOpenMenu = () => {
  let menu = document.querySelector("#nav");
  let menuItem = menu.querySelector(".aside-menu-lectures");
  let arrowItem = menu.querySelector(".arrow");
  if (menuItem) {
    arrowItem.style.display = "block";
  }

  menu.onclick = (e) => {
    e.preventDefault();
    if (menuItem) {
      if (menuItem.classList.contains("aside-menu-lectures__active")) {
        menuItem.classList.toggle("aside-menu-lectures__active");
        arrowItem.style.transform = "rotate(135deg)";
      } else {
        menuItem.classList.toggle("aside-menu-lectures__active");
        arrowItem.style.transform = "rotate(-45deg)";
      }
    } else {
      console.log("Click без выпадающего меню");
    }
  };
};

//Дз 8
const lectionFilter = () => {
  let allLecturesBlock = document.querySelector("#allLectures");
  let filterLecturesCard = [];
  let allLecturesCard = [];
  let allCard = document.querySelectorAll(".card");
  let buttonLecture = document.querySelector("#lecture-button");
  let htmlButtom = document.querySelector("#html-button");
  let cssButtom = document.querySelector("#css-button");
  let jsButtom = document.querySelector("#js-button");
  allLecturesCard = [...allCard];

  const lectureWord = (value, words) => {
    value = Math.abs(value) % 100;
    var num = value % 10;
    if (value > 10 && value < 20) return words[2];
    if (num > 1 && num < 5) return words[1];
    if (num == 1) return words[0];
    return words[2];
  };

  window.onload = () => {
    const resultLectureWord = lectureWord(allLecturesCard.length, [
      "лекция",
      "лекции",
      "лекций",
    ]);
    buttonLecture.textContent = `${allLecturesCard.length} ${resultLectureWord}`;

    for (let i = 0; i < allLecturesCard.length; i++) {
      const allCardClone = allLecturesCard[i].cloneNode(true);
      allLecturesBlock.appendChild(allCardClone);
    }
  };

  const renderCards = (typeGroup) => {
    filterLecturesCard = allLecturesCard.filter(
      (card) => card.dataset.group === typeGroup
    );

    while (allLecturesBlock.firstChild) {
      allLecturesBlock.removeChild(allLecturesBlock.firstChild);
    }

    for (let i = 0; i < filterLecturesCard.length; i++) {
      const allCardClone = filterLecturesCard[i].cloneNode(true);
      allLecturesBlock.appendChild(allCardClone);
    }
  };

  htmlButtom.onclick = () => {
    renderCards("html");
  };

  cssButtom.onclick = () => {
    renderCards("css");
  };

  jsButtom.onclick = () => {
    renderCards("JavaScript");
  };
};

//Дз 9
const objectCreate = () => {
  const cards = document.querySelectorAll(".card");
  const object = {};
  let result;
  let keyInObject;
  allCard = [...cards];
  for (let i = 0; i < cards.length; i++) {
    keyInObject = cards[i].dataset.group;
    result = allCard.filter((card) => card.dataset.group === keyInObject);
    const resultItems = result.map(
      (item) =>
        ({
          title: item.querySelector(".header-four").textContent,
          description: item.querySelector(".text-middle").textContent,
          date: item.querySelector(".text-label").textContent,
          image: item.querySelector(".card-image__item").src,
          label: item.querySelector(".card-label").textContent,
        })
    );
    object[keyInObject] = resultItems;
  }
  console.log("Finally Object", object);
};

objectCreate();
lectionFilter();
isOpenMenu();
